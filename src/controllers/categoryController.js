const CategoryDAO = require('../dao/categoryDao');

module.exports = {
  getAll: async (req, res, next) => {
    try {
      const categories = await new CategoryDAO(req.getCatalog(req.getLocale())).getAll();

      return res.json({ categories });
    } catch (err) {
      return next(err);
    }
  },
};
