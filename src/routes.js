const express = require('express');
const requireDir = require('require-dir');

const routes = express.Router();

const controllers = requireDir('./controllers');
const authenticationMiddleware = require('../middlewares/authentication');
const authorizationMiddleware = require('../middlewares/authorization');
const uploaderMiddleware = require('../middlewares/uploader');

/**
 * Auth
 */
routes.post('/auth/register', controllers.authController.register);
routes.post('/auth/login', controllers.authController.login);
routes.post('/auth/forgot_password', controllers.authController.forgotPassword);
routes.post('/auth/reset_password', controllers.authController.resetPassword);
routes.use(authenticationMiddleware);

/**
 * Admin
 */
routes.post('/admin/register', authorizationMiddleware.admin, controllers.adminController.register);
routes.get('/admin/users', authorizationMiddleware.admin, controllers.adminController.getAllUsers);

/**
 * Categories
 */
routes.get('/categories', controllers.categoryController.getAll);

/**
 * Cards
 */
routes.get('/cards/category/:categoryId', controllers.cardController.getByCategory);
routes.post('/cards', uploaderMiddleware, controllers.cardController.create);
routes.delete('/cards/:id', controllers.cardController.destroy);

module.exports = routes;
