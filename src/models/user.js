const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { JWT_SECRET } = require('../../config/env');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    'User',
    {
      firstName: DataTypes.STRING,
      lastName: DataTypes.STRING,
      gender: DataTypes.STRING,
      email: DataTypes.STRING,
      scope: DataTypes.STRING,
      password: DataTypes.STRING,
      passwordResetToken: DataTypes.STRING,
      passwordResetExpires: DataTypes.DATE,
    },
    {
      scopes: {
        fullInfo: {
          include: [{ model: sequelize.models.Handicapped }],
        },
        audit: {
          attributes: {
            exclude: ['password', 'passwordResetToken', 'passwordResetExpires'],
          },
        },
        sample: {
          attributes: ['id', 'email', 'scope'],
        },
        normal: {
          attributes: ['id', 'email', 'password'],
        },
      },
      hooks: {
        async beforeCreate(Model) {
          /* eslint-disable */
          Model.password = await bcrypt.hash(Model.password, 8);
        },
      },
    },
  );

  User.associate = models => {
    User.belongsTo(models.Handicapped);
    User.hasMany(models.Card);
  };

  User.generateToken = id => jwt.sign({ id }, JWT_SECRET);

  User.comparePassword = (password, hashPassword) => bcrypt.compare(password, hashPassword);
  User.resetPassword = password => bcrypt.hash(password, 8);

  return User;
};
