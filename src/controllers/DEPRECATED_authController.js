const express = require('express');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');

const { JWT_SECRET } = require('../../config/env');
// const mailgun = require('../../modules/mailgun');
const nodemailer = require('../../modules/nodemailer');
const UserDAO = require('../dao/DEPRECATED_userDAO');

const router = express.Router();

function generateToken(params = {}) {
  return jwt.sign(params, JWT_SECRET);
}

function sendEmail(data) {
  return new Promise((resolve, reject) => {
    nodemailer.sendMail(data, (err, result) => {
      if (err) {
        console.log(err);
        return reject();
      }

      console.log('Mensagem enviada!!!');
      resolve();
    });

    // mailgun.messages().send(data, (err, body) => {
    //   if (err) {
    //     console.log(err);
    //     return reject();
    //   }

    //   console.log('Mensagem enviada!!!');
    //   console.log('Status - ', body.message);
    //   resolve();
    // });
  });
}

function getMailTemplate(email, username, token) {
  return `
  <ul>
    <li>Nome de usuário: ${username}</li>
    <li>Email: ${email}</li>
  </ul>
  <hr>
  <br>
  <div>
    <input type="text" value="${token}" id="tokenInput" disabled>
  </div>
  <br>
  <p style="text-align: center; font-weight: bold;">Copie o token acima para alterar a senha.</p>

  <style>
  li {
    text-decoration: none;
    font-weight: bold;
    font-size: 14px;
  }
  div {
    width: 100%;
    height: fit-content;
    display: flex;
    align-items: stretch;
    height: 50px;
    font-family: Verdana, Arial, sans-serif;
  }
  input {
    border: 0;
    padding: 10px;
    text-align: center;
    text-transform: uppercase;
    letter-spacing: 1px;
    font-weight: bold;
    font-size: 2em;
    background-color: #7b1fa2 !important;
    color: #fff;
  }
  </style>
  `;
}

router.post('/register', async (req, res) => {
  let { disabled, parent } = req.body;
  const userDao = new UserDAO(req.lang);

  if (disabled.birthday) disabled.birthday = new Date(disabled.birthday);

  if (disabled.diagnostic) disabled.diagnostic = disabled.diagnostic;

  disabled.parent = parent;

  try {
    await userDao.verifyRegister(disabled);
    disabled = await userDao.register(disabled);
    disabled.password = undefined;
    disabled.parent.password = undefined;
    return res.status(201).json({
      disabled,
      message: req.lang.message.success.register,
    });
  } catch (err) {
    console.log(err);
    return res.status(400).json(err);
  }
});

router.post('/login', async (req, res) => {
  const { username, password } = req.body;

  if (!username || !password) {
    return res.status(400).json({ error: req.lang.message.error.invalidLoginReq });
  }

  try {
    ({ user } = await new UserDAO(req.lang).verifyUser({ username, password }));
    ({ user } = await new UserDAO(req.lang).getUserInfo(user));

    return res.json({
      user,
      token: generateToken({ id: user.id }),
    });
  } catch (err) {
    return res.status(400).json(err);
  }
});

router.post('/forgot_password', async (req, res) => {
  const { email, username } = req.body;

  try {
    const { user } = await new UserDAO(req.lang).checkRegister(email, username);

    const token = crypto
      .randomBytes(6)
      .toString('hex')
      .toUpperCase();
    const expiresIn = new Date();
    expiresIn.setMinutes(expiresIn.getMinutes() + 10);

    await new UserDAO(req.lang).setTokenResetPassword(user.id, token, expiresIn);

    /* ----------------- MAILGUN ----------------- */
    // const data = {
    //   from: 'dontreply@mail.vocalize.com.br',
    //   to: user.email,
    //   subject: 'Password reset',
    //   html: getMailTemplate(user.email, user.username, token)
    // };

    const data = {
      from: 'vocalize.app@outlook.com',
      to: user.email,
      subject: 'VOCALIZE - Password reset',
      html: getMailTemplate(user.email, user.username, token),
    };

    await sendEmail(data).catch(() =>
      res.status(400).json({ error: req.lang.message.error.internalError }));
    return res.json({ message: req.lang.message.success.sendMail });
  } catch (err) {
    return res.status(400).json(err);
  }
});

router.post('/reset_password', async (req, res) => {
  const { token, password } = req.body;

  try {
    const userId = await new UserDAO(req.lang).verifyResetPassword(token);
    await new UserDAO(req.lang).resetPassword(userId, password);
    return res.json({ message: req.lang.message.success.passChanged });
  } catch (err) {
    return res.status(400).json(err);
  }
});

module.exports = app => app.use('/api/auth', router);
