const formidable = require('formidable');
const uploadFile = require('../modules/aws');

module.exports = async (req, res, next) => {
  try {
    const form = new formidable.IncomingForm();
    return form.parse(req, async (err, fields, files) => {
      if (err) return next(err);

      const s3Files = await uploadFile(files, req.user.id);
      req.data = { ...fields, imageUrl: s3Files[0].Location };
      return next();
    });
  } catch (err) {
    return next(err);
  }
};
