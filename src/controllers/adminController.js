const UserDAO = require('../dao/userDao');

module.exports = {
  register: async (req, res, next) => {
    try {
      const { user } = req.body;

      if (!user) {
        return res.status(400).json({ error: req.t('message.error.invalidData') });
      }

      const userDao = await new UserDAO(req.getCatalog(req.getLocale()));
      await userDao.validateRegister(user);
      user.scope = 'admin';
      await userDao.register(user);
      return res.status(201).json({
        message: req.t('message.success.register'),
      });
    } catch (err) {
      return next(err);
    }
  },
  getAllUsers: async (req, res, next) => {
    try {
      const users = await new UserDAO(req.getCatalog(req.getLocale())).getAll();

      return res.json(users);
    } catch (err) {
      return next(err);
    }
  },
};
