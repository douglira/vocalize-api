module.exports = {
  admin: (req, res, next) => {
    try {
      const { scope } = req.user;

      if (scope !== 'admin') {
        return res.status(401).json({ error: req.t('message.error.jwtNotAuthorized') });
      }

      return next();
    } catch (err) {
      return next(err);
    }
  },
};
