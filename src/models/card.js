module.exports = (sequelize, DataTypes) => {
  const Card = sequelize.define('Card', {
    title: DataTypes.STRING,
    imageUrl: DataTypes.STRING,
  });

  Card.associate = (models) => {
    Card.belongsTo(models.Category);
    Card.belongsTo(models.User);
  };

  return Card;
};
