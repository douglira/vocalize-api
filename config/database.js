module.exports = {
  username: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_STORE,
  host: process.env.DB_HOST,
  dialect: process.env.DB_DIALECT,
  logging: false,
};
