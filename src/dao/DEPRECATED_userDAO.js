const bcrypt = require('bcryptjs');

// const ConnectionFactory = require('../ConnectionFactory');

module.exports = class UserDAO {
  constructor(lang) {
    // this._conn = ConnectionFactory.getConnection();
    this._conn = null;
    this._lang = lang;
    Object.freeze(this);
  }

  getUserInfo(user) {
    return new Promise(async (resolve, reject) => {
      try {
        if (user.level === 'disabled') {
          const { results } = await this._conn.do(
            'SELECT id as disabledId, firstName, lastName, gender, birthday, diagnostic FROM disableds WHERE user_id = ?',
            [user.id],
          );
          results[0].birthday = new Date(results[0].birthday);
          return resolve({ user: { ...user, ...results[0] } });
        }
        const { results } = await this._conn.do(
          'SELECT id as parentId, firstName, lastName, gender FROM parents WHERE user_id = ?',
          [user.id],
        );
        return resolve({ user: { ...user, ...results[0] } });
      } catch (err) {
        console.log(err);
        return reject({ error: this._lang.message.error.internalError });
      }
    });
  }

  register(disabled) {
    return new Promise(async (resolve, reject) => {
      disabled.parent.password = await this.encryptPassword(disabled.parent.password);
      disabled.password = await this.encryptPassword(disabled.password);

      this._conn.beginTransaction(async (err) => {
        if (err) {
          console.log(err);
          return reject({ error: this._lang.message.error.internalError });
        }

        try {
          let response;

          response = await this._conn
            .do(
              "INSERT INTO users SET email = ?, username = ?, password = ?, userLevel = 'parent'",
              [disabled.parent.email, disabled.parent.username, disabled.parent.password],
            )
            .catch(err =>
              this._conn.rollback(() => {
                console.log(err);
                return reject({
                  error: this._lang.message.error.invalidData,
                });
              }));
          disabled.parent.userId = response.results.insertId;

          response = await this._conn
            .do(
              "INSERT INTO users SET email = ?, username = ?, password = ?, userLevel = 'disabled'",
              [disabled.parent.email, disabled.username, disabled.password],
            )
            .catch(err =>
              this._conn.rollback(() => {
                console.log(err);
                return reject({
                  error: this._lang.message.error.invalidData,
                });
              }));
          disabled.userId = response.results.insertId;

          response = await this._conn
            .do(
              `
            INSERT INTO parents
            SET firstName = ?, lastName = ?, gender = ?, user_id = ?
            `,
              [
                disabled.parent.firstName,
                disabled.parent.lastName,
                disabled.parent.gender,
                disabled.parent.userId,
              ],
            )
            .catch(err =>
              this._conn.rollback(() => {
                console.log(err);
                return reject({
                  error: this._lang.message.error.invalidData,
                });
              }));
          disabled.parent.id = response.results.insertId;

          response = await this._conn
            .do(
              `
          INSERT INTO disableds
          SET firstName = ?, lastName = ?, gender = ?, birthday = ?,
          diagnostic = ?, parent_id = ?, user_id = ?
          `,
              [
                disabled.firstName,
                disabled.lastName,
                disabled.gender,
                disabled.birthday,
                disabled.diagnostic,
                disabled.parent.id,
                disabled.userId,
              ],
            )
            .catch(err =>
              this._conn.rollback(() => {
                console.log(err);
                return reject({
                  error: this._lang.message.error.invalidData,
                });
              }));
          disabled.id = response.results.insertId;

          this._conn.commit((err) => {
            if (err) {
              return this._conn.rollback(() => {
                console.log(err);
                return reject({
                  error: this._lang.message.error.internalError,
                });
              });
            }

            return resolve(disabled);
          });
        } catch (err) {
          console.log(err);
          return reject({
            error: this._lang.message.error.internalError,
          });
        }
      });
    });
  }

  verifyRegister(disabled) {
    return new Promise(async (resolve, reject) => {
      try {
        const { results } = await this._conn.do('SELECT email FROM users WHERE email = ?', [
          disabled.parent.email,
        ]);

        if (results && results.length !== 0) {
          return reject({ error: this._lang.message.error.emailHasRegistered });
        }

        const response = await this._conn.do(
          'SELECT username FROM users WHERE username IN (?, ?)',
          [disabled.parent.username, disabled.username],
        );

        if (response.results.length > 0) {
          return reject({ error: this._lang.message.error.existingUsername });
        }

        return resolve();
      } catch (err) {
        console.log(err);
        return reject({
          error: this._lang.message.error.internalError,
        });
      }
    });
  }

  verifyUser(user) {
    return new Promise(async (resolve, reject) => {
      try {
        const { results } = await this._conn
          .do('SELECT id, username, password, userLevel FROM users WHERE username = ?', [
            user.username,
          ])
          .catch((err) => {
            console.log(err);
            return reject({ error: this._lang.message.error.internalError });
          });

        if (results && results.length === 0) {
          return reject({ error: this._lang.message.error.userNotFound });
        }

        if (!(await this.verifyPassword(user.password, results[0].password))) {
          return reject({ error: this._lang.message.error.invalidPassword });
        }

        user.id = results[0].id;
        user.level = results[0].userLevel;
        user.password = undefined;
        return resolve({ user });
      } catch (err) {
        console.log(err);
        return reject({ error: this._lang.message.error.internalError });
      }
    });
  }

  checkRegister(email, username) {
    return new Promise(async (resolve, reject) => {
      try {
        const { results } = await this._conn.do(
          'SELECT * FROM users WHERE email = ? AND username = ?',
          [email, username],
        );

        if (results && results.length === 0) {
          return reject({ error: this._lang.message.error.userNotFound });
        }
        return resolve({ user: results[0] });
      } catch (err) {
        console.log(err);
        return reject({ error: this._lang.message.error.internalError });
      }
    });
  }

  encryptPassword(password) {
    return bcrypt.hash(password, 5);
  }

  verifyPassword(password, cryptedPassword) {
    return bcrypt.compare(password, cryptedPassword);
  }

  setTokenResetPassword(userId, token, expiresIn) {
    return new Promise(async (resolve, reject) => {
      try {
        await this._conn.do(
          'UPDATE users SET passwordResetToken = ?, passwordResetExpires = ? WHERE id = ?',
          [token, expiresIn, userId],
        );
        return resolve();
      } catch (err) {
        console.log(err);
        return reject({ error: this._lang.message.error.internalError });
      }
    });
  }

  verifyResetPassword(token) {
    return new Promise(async (resolve, reject) => {
      try {
        const { results } = await this._conn.do(
          'SELECT id, passwordResetExpires FROM users WHERE passwordResetToken = ?',
          [token],
        );
        if (results && results.length === 0) {
          return reject({ error: this._lang.message.error.invalidToken });
        }

        const now = new Date();
        const expiresIn = new Date(results[0].passwordResetExpires);

        if (now > expiresIn) {
          return reject({
            error: this._lang.message.error.expiredToken,
          });
        }

        return resolve(results[0].id);
      } catch (err) {
        console.log(err);
        return reject({ error: this._lang.message.error.internalError });
      }
    });
  }

  resetPassword(userId, password) {
    return new Promise(async (resolve, reject) => {
      try {
        password = await this.encryptPassword(password);
        await this._conn.do(
          'UPDATE users SET password = ?, passwordResetToken = null, passwordResetExpires = null WHERE id = ?',
          [password, userId],
        );
        return resolve();
      } catch (err) {
        console.log(err);
        return reject({ error: this._lang.message.error.internalError });
      }
    });
  }
};
