const { Card } = require('../models');

module.exports = class UserDAO {
  constructor(i18n) {
    this.lang = i18n;
  }

  create(newCard, userId, categoryId) {
    return new Promise(async (resolve, reject) => {
      try {
        const card = await Card.create({
          ...newCard,
          CategoryId: categoryId,
          UserId: userId,
        });
        return resolve(card);
      } catch (err) {
        // console.log(err);
        return reject(new Error(this.lang.message.error.internalError));
      }
    });
  }

  getByCategory(categoryId, userId) {
    return new Promise(async (resolve, reject) => {
      try {
        const cards = await Card.findAll({ where: { CategoryId: categoryId, UserId: userId } });

        return resolve(cards);
      } catch (err) {
        return reject(new Error(this.message.error.internalError));
      }
    });
  }

  destroy(id) {
    return new Promise(async (resolve, reject) => {
      try {
        await Card.destroy({ where: { id } });

        return resolve();
      } catch (err) {
        return reject(new Error(this.message.error.internalError));
      }
    });
  }
};
