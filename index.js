const app = require('./config/server');

const port = process.env.SERVER_PORT;
app.listen(port, () => console.log(`Server listening at port ${port}`));
