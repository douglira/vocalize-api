module.exports = (sequelize, DataTypes) => {
  const Handicapped = sequelize.define(
    'Handicapped',
    {
      firstName: DataTypes.STRING,
      lastName: DataTypes.STRING,
      gender: DataTypes.STRING,
      birthday: DataTypes.DATE,
      diagnostic: DataTypes.STRING,
    },
    {
      defaultScope: {
        attributes: {
          exclude: ['createdAt', 'updatedAt', 'UserId'],
          required: false,
        },
      },
      scopes: {},
    },
  );

  Handicapped.associate = (models) => {
    Handicapped.hasOne(models.User);
  };

  return Handicapped;
};
