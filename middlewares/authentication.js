const jwt = require('jsonwebtoken');
const { promisify } = require('util');
const UserDAO = require('../src/dao/userDao');
const { JWT_SECRET } = require('../config/env');

module.exports = async (req, res, next) => {
  try {
    const authHeader = req.headers.authorization;
    if (!authHeader) return res.status(401).json({ error: req.t('message.error.jwtNotProvided') });

    const parts = authHeader.split(' ');

    if (!(parts.length === 2)) {
      return res.status(401).json({ error: req.t('message.error.jwtInvalidFormat') });
    }

    const [scheme, token] = parts;

    if (!/^Bearer$/i.test(scheme)) {
      return res.status(401).json({ error: req.t('message.error.jwtErrorFormat') });
    }

    if (!token) {
      return res.status(401).json({ error: req.t('message.error.jwtNotProvided') });
    }

    const { id } = await promisify(jwt.verify)(token, JWT_SECRET, { ignoreExpiration: true });

    const user = await new UserDAO(req.getCatalog(req.getLocale())).checkUserExists(id);

    req.user = user;

    return next();
  } catch (err) {
    return next(err);
  }
};
