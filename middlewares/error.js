module.exports = (err, req, res, _next) => {
  if (process.env.NODE_ENV === 'development') {
    console.log('ERROR MIDDLEWARE --> ', err);
  }

  if (err.message === req.tn('message.error.internalError')) {
    return res.status(500).json({ error: req.tn('message.error.internalError') });
  }

  if (err.message === 'invalid signature') {
    return res.status(401).json({ error: req.t('message.error.jwtInvalid') });
  }

  return res.status(400).json({ error: err.message });
};
