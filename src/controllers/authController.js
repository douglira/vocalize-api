const crypto = require('crypto');

const sendMail = require('../../modules/nodemailer');
const UserDAO = require('../dao/userDao');

module.exports = {
  register: async (req, res, next) => {
    try {
      const { guardian, handicapped, user } = req.body;
      const userDao = new UserDAO(req.getCatalog(req.getLocale()));

      if (handicapped.birthday) handicapped.birthday = new Date(handicapped.birthday);
      if (handicapped.diagnostic) handicapped.diagnostic = handicapped.diagnostic;

      await userDao.validateRegister(user);
      user.scope = 'user';
      await userDao.register(user, guardian, handicapped);
      return res.status(201).json({
        message: req.t('message.success.register'),
      });
    } catch (err) {
      return next(err);
    }
  },
  login: async (req, res, next) => {
    try {
      const { email, password } = req.body;
      const userDao = new UserDAO(req.getCatalog(req.getLocale()));

      if (!email || !password) {
        return res.status(400).json({ error: req.t('message.error.invalidLoginReq') });
      }

      const user = await userDao.authenticate({ email, password });
      return res.json(user);
    } catch (err) {
      return next(err);
    }
  },
  forgotPassword: async (req, res, next) => {
    try {
      const { email } = req.body;
      const userDao = new UserDAO(req.getCatalog(req.getLocale()));
      const user = await userDao.verifyRegister(email);
      const token = crypto
        .randomBytes(7)
        .toString('hex')
        .toUpperCase();
      const expiresIn = new Date();

      expiresIn.setMinutes(expiresIn.getMinutes() + 10);
      await userDao.setTokenForgotPassword(user, token, expiresIn);
      sendMail({
        from: 'Vocalize App <vocalize@example.com>',
        to: email,
        subject: 'VOCALIZE - Password reset',
        template: `forgotPass/index.${req.getLocale()}`,
        context: {
          email,
          token,
        },
      });
      return res.json({ message: req.t('message.success.sendMail') });
    } catch (err) {
      return next(err);
    }
  },
  resetPassword: async (req, res, next) => {
    try {
      const { token, password } = req.body;
      const userDao = new UserDAO(req.getCatalog(req.getLocale()));
      const user = await userDao.verifyResetPassword(token);
      await userDao.resetPassword(user, password);

      return res.json({ message: req.t('message.success.passChanged') });
    } catch (err) {
      return next(err);
    }
  },
};
