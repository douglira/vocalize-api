CREATE TABLE users (
	id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    username varchar(20) NOT NULL UNIQUE,
    password varchar(255) NOT NULL,
    email varchar(50) NOT NULL,
    passwordResetToken varchar(255),
    passwordResetExpires DATETIME,
    userLevel varchar(15) NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMP NOT NULL DEFAULT NOW() ON UPDATE NOW() 
);

CREATE TABLE parents (
	id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    firstName varchar(20) NOT NULL,
    lastName varchar(20) NOT NULL,
    gender char NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMP NOT NULL DEFAULT NOW() ON UPDATE NOW(),
    user_id int,
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);

CREATE TABLE disableds (
	id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    firstName varchar(20) NOT NULL,
    lastName varchar(20) NOT NULL,
    gender char NOT NULL,
    birthday date,
    diagnostic varchar(30),
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMP NOT NULL DEFAULT NOW() ON UPDATE NOW(),
    parent_id int,
    user_id int, 
    FOREIGN KEY (parent_id) REFERENCES parents(id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);

DELETE FROM users WHERE id > 1;
DELETE FROM parents WHERE id > 1;
DELETE FROM disableds WHERE id > 1;