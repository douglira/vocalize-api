const CardDAO = require('../dao/cardDao');

module.exports = {
  create: async (req, res, next) => {
    try {
      const newCard = {
        title: req.data.title,
        imageUrl: req.data.imageUrl,
      };

      const cardDao = new CardDAO(req.getCatalog(req.getLocale()));

      const card = await cardDao.create(newCard, req.user.id, req.data.categoryId);

      return res.json(card);
    } catch (err) {
      return next(err);
    }
  },

  getByCategory: async (req, res, next) => {
    try {
      const { categoryId } = req.params;

      if (!categoryId) {
        return res.status(400).json({ error: req.t('message.error.missingParams') });
      }

      const cardDao = new CardDAO(req.getCatalog(req.getLocale()));
      const cards = await cardDao.getByCategory(categoryId, req.user.id);

      if (!cards || cards.length === 0) {
        return res.status(400).json({ error: req.t('message.error.cardsNotFoundByCategory') });
      }

      return res.json(cards);
    } catch (err) {
      return next(err);
    }
  },

  destroy: async (req, res, next) => {
    try {
      const { id } = req.params;

      if (!id) {
        return res.status(400).json({ error: req.t('message.error.missingParams') });
      }

      await new CardDAO(req.getCatalog(req.getLocale())).destroy(id);

      return res.json();
    } catch (err) {
      return next(err);
    }
  },
};
