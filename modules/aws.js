const Promise = require('bluebird');
const fs = Promise.promisifyAll(require('fs'));
const AWS = require('aws-sdk');

const Bucket = process.env.AWS_BUCKET;
const folder = process.env.AWS_FOLDER_CARDS;

const s3bucket = new AWS.S3({ params: { Bucket } });

const upload = params =>
  new Promise((resolve, reject) => {
    s3bucket.upload(params, (err, data) => {
      if (err) return reject(err);

      return resolve(data);
    });
  });

module.exports = (files, userId) => {
  const s3files = Object.keys(files).map(async (fileName) => {
    const file = files[fileName];
    const data = await fs.readFileAsync(file.path);

    const params = {
      Key: `${folder}/${userId}-${fileName}.png`,
      Body: data,
    };

    return upload(params);
  });

  return Promise.all(s3files);
};

/*
The following example deletes an object from an S3 bucket.

var params = {
  Bucket: "examplebucket",
  Key: "objectkey.jpg"
 };
 s3.deleteObject(params, function(err, data) {
   if (err) console.log(err, err.stack); // an error occurred
   else     console.log(data);           // successful response

   data = {
   }

 });
*/
