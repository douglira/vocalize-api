require('dotenv').config();
const app = require('express')();
const bodyParser = require('body-parser');
const cors = require('cors');
const i18n = require('./i18n');

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(i18n.init);

app.use('/api', require('../src/routes'));
app.use(require('../middlewares/error'));

module.exports = app;
