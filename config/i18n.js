const i18n = require('i18n');
const path = require('path');

i18n.configure({
  locales: ['en', 'pt'],
  directory: path.resolve('i18n', 'locales'),
  objectNotation: true,
  updateFiles: false,
  logErrorFn: console.log,
  api: {
    __: 't',
    __n: 'tn',
  },
});

module.exports = i18n;
