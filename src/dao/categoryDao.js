const { Category } = require('../models');

module.exports = class UserDAO {
  constructor(i18n) {
    this.lang = i18n;
  }

  getAll() {
    return new Promise(async (resolve, reject) => {
      try {
        const categories = await Category.findAll();
        return resolve(categories);
      } catch (err) {
        // console.log(err);
        return reject(new Error(this.lang.message.error.internalError));
      }
    });
  }
};
