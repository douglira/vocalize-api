module.exports = (sequelize, DataTypes) => {
  const Category = sequelize.define('Category', {
    title: DataTypes.STRING,
    imageUrl: DataTypes.STRING,
  });

  Category.associate = (models) => {
    Category.hasMany(models.Card);
  };

  return Category;
};
