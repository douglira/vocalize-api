const { User, Handicapped } = require('../models');

module.exports = class UserDAO {
  constructor(i18n) {
    this.lang = i18n;
  }

  register(user, guardian, handicapped) {
    return new Promise(async (resolve, reject) => {
      try {
        if (!handicapped || !guardian) {
          await User.create({ ...user });

          return resolve();
        }

        await User.create(
          {
            ...user,
            ...guardian,
            Handicapped: {
              ...handicapped,
            },
          },
          {
            include: [Handicapped],
          },
        );
        return resolve();
      } catch (err) {
        // console.log(err);
        return reject(new Error(this.lang.message.error.internalError));
      }
    });
  }

  validateRegister({ email }) {
    return new Promise(async (resolve, reject) => {
      try {
        if (await User.findOne({ where: { email } })) {
          return reject(new Error(this.lang.message.error.emailHasRegistered));
        }

        return resolve();
      } catch (err) {
        // console.log(err);
        return reject(new Error(this.lang.message.error.internalError));
      }
    });
  }

  verifyRegister(email) {
    return new Promise(async (resolve, reject) => {
      try {
        const user = await User.scope('sample').findOne({ where: { email } });

        if (!user) {
          return reject(new Error(this.lang.message.error.userNotFound));
        }

        return resolve(user);
      } catch (err) {
        // console.log(err);
        return reject(new Error(this.lang.message.error.internalError));
      }
    });
  }

  authenticate({ email, password }) {
    return new Promise(async (resolve, reject) => {
      try {
        const sampleUser = await User.scope('normal').findOne({ where: { email } });

        if (!sampleUser) {
          return reject(new Error(this.lang.message.error.userNotFound));
        }

        if (!(await User.comparePassword(password, sampleUser.password))) {
          return reject(new Error(this.lang.message.error.invalidPassword));
        }

        const user = await User.scope('sample', 'fullInfo').findOne({
          where: { id: sampleUser.id },
        });
        const token = User.generateToken(user.id);
        return resolve({ user, token });
      } catch (err) {
        // console.log(err);
        return reject(new Error(this.lang.message.error.internalError));
      }
    });
  }

  setTokenForgotPassword(user, token, expiresIn) {
    return new Promise(async (resolve, reject) => {
      try {
        await User.update(
          {
            passwordResetToken: token,
            passwordResetExpires: new Date(expiresIn),
          },
          {
            where: { ...user.dataValues },
          },
        );
        return resolve();
      } catch (err) {
        // console.log(err);
        return reject(new Error(this.lang.message.error.internalError));
      }
    });
  }

  verifyResetPassword(token) {
    return new Promise(async (resolve, reject) => {
      try {
        const user = await User.findOne({
          attributes: ['id', 'passwordResetExpires'],
          where: { passwordResetToken: token },
        });

        if (!user) {
          return reject(new Error(this.lang.message.error.invalidToken));
        }

        const now = new Date();
        const expiresIn = new Date(user.passwordResetExpires);

        if (now > expiresIn) {
          return reject(new Error(this.lang.message.error.expiredToken));
        }

        return resolve(user);
      } catch (err) {
        // console.log(err);
        return reject(new Error(this.lang.message.error.internalError));
      }
    });
  }

  resetPassword(user, password) {
    return new Promise(async (resolve, reject) => {
      try {
        const newPassword = await User.resetPassword(password);
        await User.update(
          {
            password: newPassword,
            passwordResetToken: null,
            passwordResetExpires: null,
          },
          {
            where: { id: user.id },
          },
        );

        return resolve();
      } catch (err) {
        // console.log(err);
        return reject(new Error(this.lang.message.error.internalError));
      }
    });
  }

  checkUserExists(id) {
    return new Promise(async (resolve, reject) => {
      try {
        const user = await User.scope('sample').findOne({ where: { id } });

        if (!user) {
          return reject(new Error(this.lang.message.error.jwtNotAuthorized));
        }

        return resolve(user);
      } catch (err) {
        // console.log(err);
        return reject(new Error(this.lang.message.error.internalError));
      }
    });
  }

  getAll() {
    return new Promise(async (resolve, reject) => {
      try {
        const users = await User.scope('audit').findAll();

        return resolve(users);
      } catch (err) {
        return reject(new Error(this.lang.message.error.internalError));
      }
    });
  }
};
